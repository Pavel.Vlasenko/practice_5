public class practice_5 {
    public static void main(String[] args) {
        iPhone11 iphone11 = new iPhone11("Apple Iphone11 Pro", "black", "14.5", 3, 11);
        iPhone8 iphone8 = new iPhone8("Apple Iphone8+", "red", "9.11", 9);

        Pixel pixel = new Pixel("pixel 1", "white", "10.1", 4);
        Samsung samsung = new Samsung("samsung galaxy j5", "blue", "20", 8);

        iphone11.printInfo();
        System.out.println();
        iphone8.printInfo();
        System.out.println();

        pixel.printInfo();
        System.out.println();
        samsung.printInfo();
    }
}

abstract class Phone {
    public String model;
    public String color;
    public String OSversion;

    Phone(String model, String color, String OSversion) {
        this.model = model;
        this.color = color;
        this.OSversion = OSversion;
    }
}

class Apple extends Phone {
    Apple(String model, String color, String OSversion) {
        super(model, color, OSversion);
    }

    void printIOSversion() {
        System.out.printf("IOS version: %s\t", this.OSversion);
    }
}

class Android extends Phone {
    Android(String model, String color, String OSversion) {
        super(model, color, OSversion);
    }

    void printOSversion() {
        System.out.printf("Android version: %s\t", this.OSversion);
    }
}

class iPhone11 extends Apple {
    public int countCameras;
    public int diagonal;

    iPhone11(String model, String color, String OSversion, int countCameras, int diagonal) {
        super(model, color, OSversion);
        this.countCameras = countCameras;
        this.diagonal = diagonal;
    }

    @java.lang.Override
    void printIOSversion() {
        super.printIOSversion();
    }

    void printInfo() {
        System.out.printf("Model: %s\t", this.model);
        System.out.printf("Color: %s\t", this.color);
        System.out.printf("Count cameras: %s\t", this.countCameras);
        System.out.printf("Diagonal: %s\t", this.diagonal);
        this.printIOSversion();
    }
}

class iPhone8 extends Apple {
    public int diagonal;

    iPhone8(String model, String color, String OSversion, int diagolnal) {
        super(model, color, OSversion);
        this.diagonal = diagonal;

    }

    @java.lang.Override
    void printIOSversion() {
        super.printIOSversion();
    }

    void printInfo() {
        System.out.printf("Model: %s\t", this.model);
        System.out.printf("Color: %s\t", this.color);
        System.out.printf("Diagonal: %s\t", this.diagonal);
        this.printIOSversion();
    }
}

class Pixel extends Android {
    public int ram;

    Pixel(String model, String color, String OSversion, int ram) {
        super(model, color, OSversion);
        this.ram = ram;
    }

    @java.lang.Override
    void printOSversion() {
        super.printOSversion();
    }

    void printInfo() {
        System.out.printf("Model: %s\t", this.model);
        System.out.printf("Color: %s\t", this.color);
        System.out.printf("Ram: %s\t", this.ram);
        this.printOSversion();
    }
}

class Samsung extends Android {
    public int Battary;

    Samsung(String model, String color, String OSversion, int Battary) {
        super(model, color, OSversion);
        this.Battary = Battary;
    }

    @java.lang.Override
    void printOSversion() {
        super.printOSversion();
    }

    void printInfo() {
        System.out.printf("Model: %s\t", this.model);
        System.out.printf("Color: %s\t", this.color);
        System.out.printf("Battary: %s\t", this.Battary);
        this.printOSversion();
    }
}